import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Heroe } from '../../interfaces/heroe.interface';
import { HeroesService } from '../../services/heroes.service';
import {switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styles: [
  ]
})
export class HeroeComponent implements OnInit {

  heroe!:Heroe;

  constructor(
     private activeRouter: ActivatedRoute, 
     private heroesService: HeroesService,
     private router: Router) { }

  ngOnInit(): void {
    // Leer el id del heroe y mostrar en consola // ActiveRoute
    // SIN RXJS
    // this.activeRouter.params.subscribe(({id}) => {
    //   console.log(id)
    //   this.heroesService.getHeroePorId(id).subscribe(resp => {
    //     this.heroe = resp;
    //   })
      
    // })

    //CON RJXS
    this.activeRouter.params
      .pipe(
        switchMap( ({id}) => this.heroesService.getHeroePorId(id))
      )
      .subscribe(heroe => this.heroe = heroe);
  }

  regresar() {
    this.router.navigate(['heroes/listado'])
  }

}
